#!/bin/sh

exec mplayer -loop 0 -fs -nosound -vf scale -zoom xy     \
             -really-quiet -nostop-xscreensaver          \
             /usr/share/kali-screensaver/hollywood-activate.mp4
